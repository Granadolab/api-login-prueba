<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserController extends Controller
{
    protected function getUser()
    {
        return User::all();
    }


    protected function registerUser(Request $request)
    {
        //$this->validator($request->all())->validate();

        if ($request['email']===null || $request['password']===null) {
            return 500;
        }
        event(new Registered($user = $this->createUser($request->all())));

        return $user;

    }


    protected function createUser(array $data)
    {


        return User::create([
            'name' => 'user_test',
            'email' => $data['email'],
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'password' => Hash::make($data['password']),
        ]);
    }


    protected function accessUser(Request $request)
    {

        if ($request['email']===null || $request['password']===null) {
            return 409;
        }


        $finduser=User::where([['email', $request['email']]])
        ->first();

        $hashedPassword=$finduser->password;




        if($finduser && Hash::check($request['password'], $hashedPassword)){

            return $finduser;

        }else{

            return 500;
        }

    }


}
